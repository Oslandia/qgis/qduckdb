FORMS =	../../gui/dlg_add_duckdb_layer.ui \
	../../gui/dlg_open_parquet.ui \
	../../gui/dlg_settings.ui

SOURCES =	../../gui/dlg_add_duckdb_layer.py \
	../../gui/dlg_open_parquet.py \
	../../gui/dlg_settings.py \
	../../plugin_main.py \
	../../provider/duckdb_feature_iterator.py \
	../../provider/duckdb_feature_source.py \
	../../provider/duckdb_provider.py \
	../../provider/duckdb_provider_metadata.py \
	../../provider/duckdb_wrapper.py \
	../../provider/extension.py \
	../../provider/mappings.py \
	../../provider/models.py \
	../../toolbelt/log_handler.py \
	../../toolbelt/preferences.py

TRANSLATIONS =	qduckdb_fr.ts
