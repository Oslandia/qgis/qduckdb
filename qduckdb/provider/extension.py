# https://duckdb.org/docs/extensions/core_extensions.html#list-of-core-extensions
core_extensions = [
    "arrow",
    "autocomplete",
    "aws",
    "azure",
    "delta",
    "excel",
    "fts",
    "httpfs",
    "iceberg",
    "icu",
    "inet",
    "jemalloc",
    "json",
    "mysql",
    "parquet",
    "postgres",
    "sqlite",
    "substrait",
    "tpcds",
    "tpch",
    "vss",
]

# https://duckdb.org/2024/07/05/community-extensions.html#published-extensions
community_extensions = [
    "crypto",
    "h3",
    "lindel",
    "prql",
    "scrooge",
    "shellfs",
]
